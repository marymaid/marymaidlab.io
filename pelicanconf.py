#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


# Uncomment following line if you want document-relative URLs when developing
from typing import Dict, Any
RELATIVE_URLS = True

# Masthead
AUTHOR = "The MaryMaid Company"  # Default author.
SITENAME = "The MaryMaid Company"  # The site's name.
SITESUBTITLE = "Enhance every Human life."  # Facts.
SITEURL = "https://marymaid.vip"  # The site's URL.
GITHUB_URL = "https://gitlab.com/marymaid"  # They should merge and do all dvcs's.

# TCP/IP
BIND = ""  # Listen IP
PORT = 8000  # Listen Port

# Date/Time
TIMEZONE = "America/Toronto"  # RSS/Atom
DEFAULT_DATE = "fs"  # Use file system timestamp.
DEFAULT_DATE_FORMAT = "%a %d %B %Y"  # See datetime.datetime.
DATE_FORMATS = {}  # ex: {'jp': ('ja_JP', '%Y-%m-%d(%a)')}
WITH_FUTURE_DATES = True  # Future? Draft :

# Translation
DEFAULT_LANG = "en"
# LOCALE = ()                           # Default = System.
ARTICLE_TRNASLATION_ID = "slug"  # Identify translation via attribute.
PAGE_TRANSLATION_ID = "slug"  # Identify translation via attribute.

# Organization
USE_FOLDER_AS_CATEGORY = True  # False? Fallback: DEFAULT_CATEGORY.
DEFAULT_CATEGORY = "misc"  # The category to fallback on.
DISPLAY_CATEGORIES_ON_MENU = True  # Templates may not honor this.
SUMMARY_MAX_LENGTH = 50  # Wordmax; Fallback for :summary:.

# Pathing
PATH = "content"  # The content directory.
IGNORE_FILES = ["theme"]  # Ignored globs.
OUTPUT_PATH = "public"  # The output path.
DELETE_OUTPUT_DIRECTORY = False  # True? USE WITH CAUTION.
OUTPUT_RETENTION = [".git"]  # Kept in the output directory.
OUTPUT_SOURCES = False  # Copy original formats?
OUTPUT_SOURCES_EXTENSION = ".text"  # Copy originals as...
ARTICLE_PATHS = [""]  # Relative to PATH.
ARTICLE_EXCLUDES = [""]  # Excluded directories.
PAGE_PATHS = ["pages"]  # Relative to PATH.
PAGE_EXCLUDES = [""]  # Excluded directories.
SLUGIFY_SOURCE = "title"  # 'title': {title}|'basename': {mtime}
STATIC_PATHS = ["theme"]  # Static files relative to PATH.
STATIC_EXCLUDES = [""]  # Excluded static directories.
STATIC_CREATE_LINKS = False  # True: Symlink | False: Copy.
STATIC_CHECK_IF_MODIFIED = True  # Compare content/output mtimes.

# *_SAVE_AS defaults to *_URL.
AUTHOR_URL = "author/{slug}/"
AUTHOR_SAVE_AS = "author/{slug}/index.html"
AUTHORS_SAVE_AS = "authors/index.html"

CATEGORY_URL = "arcana/{slug}/"
CATEGORY_SAVE_AS = "arcana/{slug}/index.html"

TAG_URL = "affix/{slug}/"
TAG_SAVE_AS = "affix/{slug}/index.html"

ARTICLE_URL = "{slug}/"
ARTICLE_SAVE_AS = "{slug}/index.html"
ARTICLE_LANG_URL = "{lang}/{slug}/"
ARTICLE_LANG_SAVE_AS = "{lang}/{slug}/index.html"
DRAFT_URL = "draft/{slug}/"
DRAFT_SAVE_AS = "draft/{slug}/index.html"
DRAFT_LANG_URL = "draft/{lang}/{slug}/"
DRAFT_LANG_SAVE_AS = "draft/{lang}/{slug}/index.html"

PAGE_URL = "aspect/{slug}/"
PAGE_SAVE_AS = "aspect/{slug}/index.html"
PAGE_LANG_URL = "aspect/{lang}/{slug}/"
PAGE_LANG_SAVE_AS = "aspect/{lang}/{slug}/index.html"
DRAFT_PAGE_URL = "draft/aspect/{slug}/"
DRAFT_PAGE_SAVE_AS = "draft/aspect/{slug}/index.html"
DRAFT_PAGE_LANG_URL = "draft/aspect/{lang}/{slug}/"
DRAFT_PAGE_LANG_SAVE_AS = "draft/aspect/{lang}/{slug}/index.html"

# These require the {url} placeholder in PAGINATION_PATTERNS.
YEAR_ARCHIVE_URL = ""
YEAR_ARCHIVE_SAVE_AS = ""
MONTH_ARCHIVE_URL = ""
MONTH_ARCHIVE_SAVE_AS = ""
DAY_ARCHIVE_URL = ""
DAY_ARCHIVE_SAVE_AS = ""

# Pagination
# DEFAULT_PAGINATION = False            # Disable pagination.
DEFAULT_PAGINATION = 33  # Articles per page, excluding orphans.
DEFAULT_ORPHANS = 33  # Articles allowed on the last page.
PAGINATED_TEMPLATES = {  # 'Template': Integer per page.
    "index": 33,
    "tag": 33,
    "category": 33,
    "author": 33,
    "article": 33,
}
PAGINATION_PATTERNS = (
    # (minimum_page, 'page_url', 'save_as')
    # {url}: *_URL; {save_as}: *_SAVE_AS.
    # {base_name} = {name} sans trailing /index.
    # {number} = page number.
    (1, "{url}", "{save_as}"),
    (2, "{base_name}/page/{number}", "{base_name}/page/{number}/index.html"),
)

# Plugins
PLUGIN_PATHS = ["/srv/www/pelican/plugins/"]
PLUGINS = ["asciidoc_reader", "autopages", "dateish", "neighbors"]

# Autopages
AUTHOR_PAGE_PATH = "authors"
CATEGORY_PAGE_PATH = "categories"
TAG_PAGE_PATH = "tags"

# Themes
THEME = "content/theme"  # See pelican-themes installer.
THEME_STATIC_DIR = "theme"  # Output destination.
THEME_STATIC_PATHS = ["static"]  # Static theme paths to copy.
THEME_TEMPLATES_OVERRIDES = []  # Directory for template overrides.
CSS_FILE = "style.css"
TEMPLATE_PAGES = {  # Custom Jinja templates.
    # 'folder/jinja_template.html': 'jinja_template.html'
}

# Categories
CATEGORY_WIDGET_NAME = "Arcana"

# Pages
PAGE_WIDGET_NAME = "Aspects"

# Blogroll
LINKS_WIDGET_NAME = "All-Stars"
LINKS = (
    ("#NBYMP", "https://nationalbasketballmentorship.ca"),
    ("Freedom to Fly", "https://gatewayto.life"),
    ("Port Royal Ltd", "https://portroyal.ltd"),
)

# Social widget
SOCIAL_WIDGET_NAME = "@'s"
SOCIAL = (
    ("@nbymp", "https://facebook.com/nbymp"),
    ("@marymaid.co", "https://facebook.com/marymaid.co"),
    ("@freedomtofly.life", "https://facebook.com/freedomtofly.life"),
    ("@portroyalltd", "https://facebook.com/portroyalltd"),
)

# Affiliate Widget
AFFILIATE_WIDGET_NAME = "Affiliates"
AFFILIATES = (
    ("Moo", "https://refer.moo.com/s/high"),
    ("Candelivery", "https://candelivery.ca?ref=1061"),
)

# Tag Widget
TAG_WIDGET_NAME = "Affixes"

# Processing
CACHE_CONTENT = False  # Comes with caveats.
CONTENT_CACHING_LAYER = "reader"  # 'reader' | 'generator'
CACHE_PATH = "cache"  # Caching directory.
GZIP_CACHE = True  # gzip?
LOAD_CONTENT_CACHE = False  # True? Load unmodified from cache.

JINJA_ENVIRONMENT = {  # Custom Jinja variables.
    # http://jinja.pocoo.org/docs/dev/api/#jinja2.Environment
}
JINJA_FILTERS = {  # Custom Jinja filters.
    # http://jinja.pocoo.org/docs/api/#custom-filters
}

TYPOGRIFY = True  # See Typogrify.
TYPOGRIFY_IGNORE_TAGS = [""]  # Tags for Typogrify to ignore.

LOG_FILTER = [
    # http://docs.getpelican.com/en/stable/settings.html#logging
]

MARKDOWN = {
    # https://python-markdown.github.io/extensions/
    # https://python-markdown.github.io/reference/#markdown
    "extension_configs": {
        "markdown.extensions.codehilite": {"css_class": "highlight"},
        "markdown.extensions.extra": {},
        "markdown.extensions.meta": {},
    },
    "output_format": "html5",
}

FORMATTED_FIELDS = ["summary"]  # Selective rST/Markdown metadata.

READERS = {}  # {'extension': FooReader}

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ATOM = None
FEED_ALL_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_ATOM_URL = None
TRANSLATION_FEED_RSS = None
TRANSLATION_FEED_RSS_URL = None
