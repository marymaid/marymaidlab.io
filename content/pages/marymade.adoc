= <i>MaryMade!</i>
:author: Witch Doctor
:status: published

*The MaryMaid Company*'s _MaryMade!_ program empowers intrepid entrepreneurs with resources, a platform and an audience.

The Catch: ::
* _Unprocessesed cannabinoids_ are *prohibited*.
* The house rate is _3.6%_. +

That's all.

In return, *The MaryMaid Company* enables proactive self-education through the distribution of curated resources tailored towards growing small businesses. *MaryMaid* also actively promotes those that have been _MaryMade!_ through its platform and various social media. Lastly, *The MaryMaid Company* believes in putting on _as many independents as possible_; this means solopreneurs, armies of two, three amigos, and family operations (big families too!).

Stay tuned for details.
